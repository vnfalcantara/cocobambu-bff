# cocobambu-bff

Aplicação de teste utilizando a MEAN stack.

### Como rodar
```sh
$ git clone
$ cd <cocobambu-bff>
$ npm install
$ npm run dev
```

### Testes de integração
collection "cocobambu_test"
```sh
$ npm t
```

### mogorestore [OPCIONAL]
- Os dados são inseridos no banco no primeiro carregamento
- Mas, caso prefira restaurar o banco através do comando "mongorestore":
```sh
$ mongorestore --db cocobambu_dev <cocobambu-bff>/mongodump/cocobambu_dev
```