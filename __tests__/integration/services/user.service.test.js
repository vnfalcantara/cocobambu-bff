const db = require('../../../app/db/mongo')
const app = require('../../../app/app')
const usersMock = require('../../__mocks__/users.json')
const newUser = require('../../__mocks__/user.new.json')

let UserService
let userService

describe('user.service', () => {

  beforeAll(async done => {
    await db.connect()
    await app.loadModels()

    UserService = require('../../../app/services/user.service')
    userService = new UserService()

    await userService.remove({})

    for (const user of usersMock)
      userService.insert(user)

    done()
  })

  describe('insert', () => {
    
    test('shold return a mongoose validation error', done => {
      const user = { ...usersMock[0] }

      userService.insert(user)
        .catch(error => done())
    })

    test('shold insert a new document', async done => {
      const data = await userService.insert(newUser)

      expect(data.name).toBe(newUser.name)
      expect(data.email).toBe(newUser.email)

      done()
    })

  })

  describe('find', () => {

    test('should return a empty array', async done => {
      const data = await userService.find({ name: 'wally' })

      expect(data).toEqual([])

      done()
    })

    test('should return all documents', async done => {
      const data = await userService.find({})

      expect(data.length).toBe(2)

      done()
    })

    test('should return one document', async done => {
      const { email } = usersMock[0]
      const data = await userService.find({ email })

      expect(data.length).toBe(1)

      done()
    })

    test('should not return the password field', async done => {
      const { email } = usersMock[0]
      const data = await userService.find({ email })

      expect(data.password).toBeUndefined

      done()
    })

    test('should return the password field', async done => {
      const { email } = usersMock[0]
      const data = await userService.find({ email }).select('+password')

      expect(data.password).toBeUndefined

      done()
    })

    test('should return one document with properties "_id" and "name"', async done => {
      const { email } = usersMock[0]
      const data = await userService.find({ email }, { name: 1 })

      expect(data[0]._id).toBeTruthy()
      expect(data[0].name).toBeTruthy()
      expect(data[0].email).toBeUndefined()

      done()
    })

  })

  describe('findOne', () => {

    test('should return null', async done => {
      const data = await userService.findOne({ email: 'test' })

      expect(data).toBeNull()

      done()
    })

    test('should return the document', async done => {
      const user = { ...usersMock[0] }
      const { email } = user
      const data = await userService.findOne({ email })

      expect(data.name).toBe(user.name)
      expect(data.email).toBe(user.email)

      done()
    })

    test('should not return the password field', async done => {
      const user = { ...usersMock[0] }
      const { email } = user
      const data = await userService.findOne({ email })

      expect(data.email).toBe(user.email)
      expect(data.password).toBeUndefined()

      done()
    })

    test('should return the password field', async done => {
      const user = { ...usersMock[0] }
      const { email } = user
      const data = await userService.findOne({ email }).select('+password')

      expect(data.email).toBe(user.email)
      expect(data.password).toBeTruthy()

      done()
    })

    test('should return the document with properties "_id" and "name"', async done => {
      const { email } = usersMock[0]
      const data = await userService.findOne({ email }, { name: 1 })

      expect(data._id).toBeTruthy()
      expect(data.name).toBeTruthy()
      expect(data.email).toBeUndefined()

      done()
    })

  })

  describe('findById', () => {

    test('should return the document', async done => {
      const user = { ...usersMock[0] }
      const { _id } = user
      const data = await userService.findById(_id)

      expect(data.name).toBe(user.name)
      expect(data.email).toBe(user.email)

      done()
    })

    test('should not return the password field', async done => {
      const user = { ...usersMock[0] }
      const { _id } = user
      const data = await userService.findById(_id)

      expect(data.email).toBe(user.email)
      expect(data.password).toBeUndefined()

      done()
    })

    test('should return the password field', async done => {
      const user = { ...usersMock[0] }
      const { _id } = user
      const data = await userService.findById(_id).select('+password')

      expect(data.email).toBe(user.email)
      expect(data.password).toBeTruthy()

      done()
    })

    test('should return the document with properties "_id" and "name"', async done => {
      const { _id } = usersMock[0]
      const data = await userService.findById(_id, { name: 1 })

      expect(data._id).toBeTruthy()
      expect(data.name).toBeTruthy()
      expect(data.email).toBeUndefined()

      done()
    })

  })

  describe('count', () => {
    
    test('should return 0', async done => {
      const count = await userService.count({ email: 'test' })

      expect(count).toBe(0)

      done()
    })

    test('should return 2', async done => {
      const count = await userService.count({})

      expect(count).toBe(2)

      done()
    })

    test('should return 1', async done => {
      const user = { ...usersMock[0] }
      const { email } = user
      const count = await userService.count({ email })

      expect(count).toBe(1)

      done()
    })

  })

  describe('update', () => {

    test('should not update', async done => {
      const update = { email: 'test' }
      const updateStatus = await userService.update({ email: 'update' }, update)

      expect(updateStatus.nModified).toBe(0)

      done()
    })

    test('should update the document', async done => {
      const oldUser = { ...usersMock[0] }
      const { _id, email } = oldUser
      const update = { name: 'test' }
      let updateStatus
      let newUser

      updateStatus = await userService.update({ email }, update)
      newUser = await userService.findById(_id)

      expect(updateStatus.nModified).toBe(1)
      expect(newUser.name).toBe(update.name)

      done()
    })

  })

  describe('remove', () => {

    test('should not remove', async done => {
      const oldCount = await userService.count({})
      const removeStatus = await userService.remove({ email: 'remove' })
      const newCount = await userService.count({})

      expect(oldCount).toBe(2)
      expect(removeStatus.deletedCount).toBe(0)
      expect(newCount).toBe(2)

      done()
    })

    test('should remove one document', async done => {
      const recipe = { ...usersMock[0] }
      const { email } = recipe
      const oldCount = await userService.count({})
      const removeStatus = await userService.remove({ email })
      const newCount = await userService.count({})

      expect(oldCount).toBe(2)
      expect(removeStatus.deletedCount).toBe(1)
      expect(newCount).toBe(1)

      done()
    })

  })

})