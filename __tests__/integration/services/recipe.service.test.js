const db = require('../../../app/db/mongo')
const app = require('../../../app/app')
const recipesMock = require('../../__mocks__/recipes.json')
let RecipeService
let recipeService

describe('recipe.service', () => {

  beforeAll(async done => {
    await db.connect()
    await app.loadModels()

    RecipeService = require('../../../app/services/recipe.service')
    recipeService = new RecipeService()

    await recipeService.remove({})

    for (const recipe of recipesMock)
      recipeService.insert(recipe)

    done()
  })

  describe('insert', () => {

    test('shold return a mongoose validation error', done => {
      const recipe = { ...recipesMock[0], name: undefined }

      recipeService.insert(recipe)
        .catch(error => done())
    })

    test('should insert a new document', async done => {
      const recipe = { ...recipesMock[0], _id: undefined }
      const data = await recipeService.insert(recipe)

      expect(data.name).toBe(recipe.name)
      expect(data.description).toBe(recipe.description)

      done()
    })

  })

  describe('find', () => {

    test('should return a empty array', async done => {
      const data = await recipeService.find({ name: 'test' })

      expect(data).toEqual([])

      done()
    })

    test('should return all documents', async done => {
      const data = await recipeService.find({})

      expect(data.length).toBe(3)

      done()
    })

    test('should return one documents', async done => {
      const { _id } = recipesMock[0]
      const data = await recipeService.find({ _id })

      expect(data.length).toBe(1)

      done()
    })

    test('should return one document with properties "_id" and "name"', async done => {
      const { _id } = recipesMock[0]
      const data = await recipeService.find({ _id }, { name: 1 })

      expect(data[0]._id).toBeTruthy()
      expect(data[0].name).toBeTruthy()
      expect(data[0].orderedAt).toBeUndefined()
      expect(data[0].createdAt).toBeUndefined()

      done()
    })

  })

  describe('findOne', () => {

    test('should return null', async done => {
      const data = await recipeService.findOne({ name: 'test' })

      expect(data).toBeNull()

      done()
    })

    test('should return the document', async done => {
      const recipe = { ...recipesMock[0] }
      const { _id } = recipe
      const data = await recipeService.findOne({ _id })

      expect(data.name).toBe(recipe.name)
      expect(data.description).toBe(recipe.description)

      done()
    })

    test('should return the document with properties "_id" and "name"', async done => {
      const { _id } = recipesMock[0]
      const data = await recipeService.findOne({ _id }, { name: 1 })

      expect(data._id).toBeTruthy()
      expect(data.name).toBeTruthy()
      expect(data.orderedAt).toBeUndefined()
      expect(data.createdAt).toBeUndefined()

      done()
    })

  })

  describe('findById', () => {

    test('should return the document', async done => {
      const recipe = { ...recipesMock[0] }
      const { _id } = recipe
      const data = await recipeService.findById(_id)

      expect(data.name).toBe(recipe.name)
      expect(data.description).toBe(recipe.description)

      done()
    })

    test('should return the document with properties "_id" and "name"', async done => {
      const { _id } = recipesMock[0]
      const data = await recipeService.findById({ _id }, { name: 1 })

      expect(data._id).toBeTruthy()
      expect(data.name).toBeTruthy()
      expect(data.orderedAt).toBeUndefined()
      expect(data.createdAt).toBeUndefined()

      done()
    })

  })

  describe('count', () => {

    test('should return 0', async done => {
      const count = await recipeService.count({ name: 'test' })

      expect(count).toBe(0)

      done()
    })

    test('should return 3', async done => {
      const count = await recipeService.count({})

      expect(count).toBe(3)

      done()
    })

    test('should return 1', async done => {
      const recipe = { ...recipesMock[0] }
      const { _id } = recipe
      const count = await recipeService.count({ _id })

      expect(count).toBe(1)

      done()
    })

  })

  describe('update', () => {

    test('should not update', async done => {
      const update = { name: 'test' }
      const updateStatus = await recipeService.update({ name: 'update' }, update)

      expect(updateStatus.nModified).toBe(0)

      done()
    })

    test('should update the document', async done => {
      const oldRecipe = { ...recipesMock[0] }
      const { _id } = oldRecipe
      const update = { name: 'test' }
      let updateStatus
      let newRecipe

      updateStatus = await recipeService.update({ _id }, update)
      newRecipe = await recipeService.findById(_id)

      expect(updateStatus.nModified).toBe(1)
      expect(newRecipe.name).toBe(update.name)

      done()
    })

  })

  describe('remove', () => {

    test('should not remove', async done => {
      const oldCount = await recipeService.count({})
      const removeStatus = await recipeService.remove({ name: 'remove' })
      const newCount = await recipeService.count({})

      expect(oldCount).toBe(3)
      expect(removeStatus.deletedCount).toBe(0)
      expect(newCount).toBe(3)

      done()
    })

    test('should remove one document', async done => {
      const recipe = { ...recipesMock[0] }
      const { _id } = recipe
      const oldCount = await recipeService.count({})
      const removeStatus = await recipeService.remove({ _id })
      const newCount = await recipeService.count({})

      expect(oldCount).toBe(3)
      expect(removeStatus.deletedCount).toBe(1)
      expect(newCount).toBe(2)

      done()
    })

  })

})