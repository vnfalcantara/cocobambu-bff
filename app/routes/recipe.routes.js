const RecipeController = require('../controllers/recipe.controller')
const recipe = new RecipeController()

module.exports = extra => {
  
  const { app, passportJWT } = extra
  const basePath = '/recipe'

  app.post(basePath, passportJWT.authenticate(), recipe.insert)
  app.get(basePath, passportJWT.authenticate(), recipe.find)
  app.get(`${basePath}/:id`, passportJWT.authenticate(), recipe.findOne)
  app.put(basePath, passportJWT.authenticate(), recipe.update)
  app.patch(basePath, passportJWT.authenticate(), recipe.update)
  app.delete(`${basePath}/:id`, passportJWT.authenticate(), recipe.remove)

}
