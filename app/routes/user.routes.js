const UserController = require('../controllers/user.controller')
const user = new UserController()

module.exports = extra => {
  
  const { app, passportJWT } = extra
  const basePath = '/user'

  app.get(`${basePath}/me`, passportJWT.authenticate(), user.me)
  app.post(`${basePath}/login`, user.login)

  app.post(basePath, passportJWT.authenticate(), user.insert)
  app.get(basePath, passportJWT.authenticate(), user.find)
  app.get(`${basePath}/:id`, passportJWT.authenticate(), user.findOne)
  app.put(basePath, passportJWT.authenticate(), user.update)
  app.patch(basePath, passportJWT.authenticate(), user.update)
  app.delete(`${basePath}/:id`, passportJWT.authenticate(), user.remove)

}
