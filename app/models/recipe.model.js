const mongoose = require('mongoose')
const Schema = mongoose.Schema

const RecipeSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  img: { type: String },
  thumbnail: { type: String },
  status: {
    code: { type: Number },
    text: { type: String }
  },
  ingredients: [{
    proportion: { type: String },
    name: { type: String },
  }],
  preparation: {
    estimatedMinutes: { type: Number },
    steps: [{
      name: { type: String },
      description: { type: String },
      completed: { type: Boolean, default: false },
    }]
  },
  orderedAt: { type: Date, default: Date.now },
  finishedAt: { type: Date },
  createdAt: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Recipe', RecipeSchema)