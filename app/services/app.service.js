class AppService {

  constructor(Model) {
    this.Model = Model
  }

  insert(data) {
    const model = new this.Model(data)
    return model.save()
  }

  find(params = {}, fields = {}, sort, skip, limit) {
    return this.Model.find(params, fields)
      .sort(sort)
      .skip(skip)
      .limit(limit)
  }

  findOne(query = {}, fields) {
    return this.Model.findOne(query, fields)
  }

  findById(_id, fields) {
    return this.Model.findOne({ _id }, fields)
  }

  count(query = {}) {
    return this.Model.countDocuments(query)
  }

  update(query, data) {
    return this.Model.update(query, data)
  }

  remove(query) {
    return this.Model.deleteMany(query)
  }

}

module.exports = AppService