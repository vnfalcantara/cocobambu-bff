const AppService = require('./app.service')
const model = require('mongoose').model('Recipe')

class RecipeService extends AppService {

  constructor() {
    super(model)
    this.model = model
  }

}

module.exports = RecipeService