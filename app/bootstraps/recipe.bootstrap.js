const RecipeService = require('../services/recipe.service')
const recipeService = new RecipeService()
const { color } = require('../constants')
const recipes = require('./data/recipes.json')

const run = async () => {

  try {
    for (const recipe of recipes) {
      const hasRecipe = await recipeService.count({ _id: recipe._id })

      if (!hasRecipe)
        recipeService.insert(recipe)
    }
  } catch (error) {
    console.log(color.RED, error)
  }

}

run()