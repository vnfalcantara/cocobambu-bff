class AppController {

  constructor(service) {
    this.service = service
    
    this.insert = this.insert.bind(this)
    this.count = this.count.bind(this)
    this.find = this.find.bind(this)
    this.findOne = this.findOne.bind(this)
    this.update = this.update.bind(this)
    this.updateMany = this.updateMany.bind(this)
    this.remove = this.remove.bind(this)
    this.removeMany = this.removeMany.bind(this)
  }

  async insert(req, res) {
    const { body } = req

    try {
      const data = await this.service.insert(body)
      res.json(data)
    } catch (error) {
      throw new Error(error)
    }
  }

  async count(req, res) {
    let { params } = req.query

    try {
      const data = await this.service.count(params)
      res.json(data)
    } catch (error) {
      throw new Error(error)
    }
  }

  async find(req, res) {
    let { params, fields, sort, skip, limit } = req.query

    try {
      const data = await this.service.find(params, fields, sort, Number(skip), Number(limit))
      res.json(data)
    } catch (error) {
      throw new Error(error)
    }
  }

  async findOne(req, res) {
    const _id = req.params.id
    const { fields } = req.query

    try {
      const data = await this.service.findOne({ _id }, fields)
      res.json(data)
    } catch (error) {
      throw new Error(error)
    }
  }

  async update(req, res) { }

  async updateMany(req, res) { }

  async remove(req, res) { }

  async removeMany(req, res) { }

}

module.exports = AppController