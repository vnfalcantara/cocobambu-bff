const AppController = require('./app.controller')
const RecipeService = require('../services/recipe.service')

const recipeService = new RecipeService()

class RecipeController extends AppController {

  constructor() {
    super(recipeService)
  }

}

module.exports = RecipeController